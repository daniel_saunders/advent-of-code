use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::path::Path;
use regex::Regex;

fn main() {
    let path = Path::new("../input");

    let file = match File::open(&path) {
        Err(why) => panic!("Couldn't open {}", why),
        Ok(file) => file
    };

    let lines = BufReader::new(file).lines();

    let re = Regex::new(r"^([0-9]+)-([0-9]+) ([a-zA-Z]): ([a-zA-Z]+)$").unwrap();

    let mut count_correct = 0;

    for line in lines {
        if let Ok(next_line) = line {
            let captures = re.captures(&next_line).unwrap();
            let min: i32 = captures.get(1).unwrap().as_str().parse().unwrap();
            let max: i32 = captures.get(2).unwrap().as_str().parse().unwrap();
            let character: char = captures.get(3).unwrap().as_str().parse().unwrap();
            let password = captures.get(4).unwrap().as_str();

            let mut count = 0;
            for c in password.chars() {
                if c == character {
                    count += 1;
                }
            }

            if count <= max && count >= min {
                count_correct += 1;
            }

        }
    }

    println!("{}", count_correct);
}
