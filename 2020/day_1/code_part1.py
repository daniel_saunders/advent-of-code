a = open('input', 'r')

current_rows = []

for line in a.readlines():
    int_line = int(line)

    for row in current_rows:
        if row + int_line == 2020:
            print(row * int_line)
            exit(0)

    current_rows.append(int_line)
