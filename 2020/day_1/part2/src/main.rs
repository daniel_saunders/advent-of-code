use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;
use std::process;

fn main() {
    let path = Path::new("../input");

    let file = match File::open(&path) {
        Err(why) => panic!("Couldn't open {}", why),
        Ok(file) => file,
    };

    let lines = BufReader::new(file).lines();

    let mut seen_lines: Vec<i32> = Vec::new();

    for line in lines {
        if let Ok(next_line) = line {
            let next_line_i: i32 = next_line.parse().unwrap();

            for seen_line_1 in &seen_lines {
                for seen_line_2 in &seen_lines {
                    if *seen_line_1 + *seen_line_2 + next_line_i == 2020 {
                        println!("{:?}", *seen_line_1 * *seen_line_2 * next_line_i);
                        process::exit(0x0100);
                    }
                }
            }

            seen_lines.push(next_line.parse().unwrap());
        }
    }
}
