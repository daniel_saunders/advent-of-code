a = open('input', 'r')

current_rows = []

for line in a.readlines():
    int_line = int(line)

    for row_a in current_rows:
        for row_b in current_rows:
            if row_a + row_b + int_line == 2020:
                print(row_a * int_line * row_b)
                exit(0)

    current_rows.append(int_line)
