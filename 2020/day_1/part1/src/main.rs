use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::path::Path;

fn main() {
    let path = Path::new("../input");

    let file = match File::open(&path) {
        Err(why) => panic!("Couldn't open {}", why),
        Ok(file) => file
    };

    let lines = BufReader::new(file).lines();

    let mut seen_lines: Vec<i32> = Vec::new();

    for line in lines {
        if let Ok(next_line) = line {
            let next_line_i: i32 = next_line.parse().unwrap();

            for seen_line in &seen_lines {
                if *seen_line + next_line_i == 2020 {
                    println!("{:?}", *seen_line * next_line_i);
                }
            }

            seen_lines.push(next_line.parse().unwrap());
        }
    }
}
