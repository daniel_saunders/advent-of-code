use std::{
    cmp::Ordering,
    collections::{BinaryHeap, HashSet},
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
struct Node {
    cost: usize,
    real_cost: usize,
    position: (usize, usize),
}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.cost.cmp(&self.cost)
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

struct Grid {
    grid: Vec<Vec<usize>>,
    width: usize,
    height: usize,
    seen: HashSet<(usize, usize)>,
}

impl Grid {
    pub fn new(path_string: &str) -> Grid {
        let path = Path::new("../input");

        let file = File::open(path).expect("Unable to open input file");

        let lines = BufReader::new(file).lines();

        let mut grid: Vec<Vec<usize>> = Vec::new();

        for (idy, line) in lines.enumerate() {
            let next_line = line.expect("Unable to read next line of input");

            for (idx, c) in next_line.chars().enumerate() {
                if idx >= grid.len() {
                    grid.push(Vec::new());
                }

                grid[idx]
                    .push(c.to_digit(10).expect("Unable to parse next digit in input") as usize);
            }
        }

        let width = grid.len();
        let height = grid[0].len();

        Grid {
            grid,
            width,
            height,
            seen: HashSet::new(),
        }
    }

    pub fn get_start_node(&mut self) -> Node {
        self.seen.insert((0, 0));
        Node {
            cost: 0,
            real_cost: 0,
            position: (0, 0),
        }
    }

    pub fn is_target_node(&self, node: &Node) -> bool {
        node.position.0 == self.width - 1 && node.position.1 == self.height - 1
    }

    fn get_heuristic_cost(&self, x: usize, y: usize) -> usize {
        0
        // (self.width - x) + (self.height - y)
    }

    pub fn get_unseen_neighbors(
        &mut self,
        current_cost: usize,
        position: (usize, usize),
    ) -> Vec<Node> {
        let mut neighbors = Vec::new();

        if position.0 > 0 && !self.seen.contains(&(position.0 - 1, position.1)) {
            let base_cost = current_cost + self.grid[position.0 - 1][position.1];
            neighbors.push(Node {
                cost: base_cost + self.get_heuristic_cost(position.0 - 1, position.1),
                position: (position.0 - 1, position.1),
                real_cost: base_cost,
            });
            self.seen.insert((position.0 - 1, position.1));
        }

        if position.1 > 0 && !self.seen.contains(&(position.0, position.1 - 1)) {
            let base_cost = current_cost + self.grid[position.0][position.1 - 1];
            neighbors.push(Node {
                cost: base_cost + self.get_heuristic_cost(position.0, position.1 - 1),
                position: (position.0, position.1 - 1),
                real_cost: base_cost,
            });
            self.seen.insert((position.0, position.1 - 1));
        }

        if position.0 < self.width - 1 && !self.seen.contains(&(position.0 + 1, position.1)) {
            let base_cost = current_cost + self.grid[position.0 + 1][position.1];
            neighbors.push(Node {
                cost: base_cost + self.get_heuristic_cost(position.0 + 1, position.1),
                position: (position.0 + 1, position.1),
                real_cost: base_cost,
            });
            self.seen.insert((position.0 + 1, position.1));
        }

        if position.1 < self.height - 1 && !self.seen.contains(&(position.0, position.1 + 1)) {
            let base_cost = current_cost + self.grid[position.0][position.1 + 1];
            neighbors.push(Node {
                cost: base_cost + self.get_heuristic_cost(position.0, position.1 + 1),
                position: (position.0, position.1 + 1),
                real_cost: base_cost,
            });
            self.seen.insert((position.0, position.1 + 1));
        }

        neighbors
    }
}

fn main() {
    let mut grid = Grid::new("../input");

    let mut next_nodes: BinaryHeap<Node> = BinaryHeap::new();

    next_nodes.push(grid.get_start_node());

    let mut answer = 0;

    loop {
        let next_node = next_nodes.pop().unwrap();

        if grid.is_target_node(&next_node) {
            answer = next_node.real_cost;
            break;
        }

        let neighbors = grid.get_unseen_neighbors(next_node.real_cost, next_node.position);
        for neighbor in neighbors {
            // println!("{:?} of {:?}", neighbor, next_node);
            next_nodes.push(neighbor);
        }
    }

    println!("Answer: {}", answer);
}
