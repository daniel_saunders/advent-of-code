use std::{
    collections::HashSet,
    fs::File,
    io::{BufRead, BufReader},
    path::Path, cmp,
};

use regex::Regex;

enum Direction {
    X,
    Y,
}

impl Direction {
    fn from_char(c: char) -> Direction {
        match c {
            'x' => Direction::X,
            'y' => Direction::Y,
            _ => panic!("Invalid char {} for direction", c),
        }
    }
}

fn fold(coords: HashSet<(usize, usize)>, fold: &(Direction, usize)) -> HashSet<(usize, usize)> {
    let mut new_set = HashSet::new();

    let direction = &fold.0;
    let fold_position = fold.1;

    for coord in coords {
        let x = coord.0;
        let y = coord.1;

        match direction {
            Direction::X => {
                if x < fold_position {
                    new_set.insert(coord.clone());
                } else if x > fold_position {
                    new_set.insert((fold_position - (x - fold_position), y));
                }
            }
            Direction::Y => {
                if y < fold_position {
                    new_set.insert(coord.clone());
                } else if y > fold_position {
                    new_set.insert((x, fold_position - (y - fold_position)));
                }
            }
        };
    }

    new_set
}

fn main() {
    let path = Path::new("../input");

    let file = File::open(path).expect("Error opening file");

    let lines = BufReader::new(file).lines();

    let coordinates_re = Regex::new("([0-9]+),([0-9]+)").unwrap();
    let fold_re = Regex::new("fold along (x|y)=([0-9]+)").unwrap();

    let mut folds: Vec<(Direction, usize)> = Vec::new();
    let mut coords: HashSet<(usize, usize)> = HashSet::new();

    for line in lines {
        let next_line = line.expect("Failed to read next line");

        if let Some(captures) = coordinates_re.captures(&next_line) {
            let x: usize = captures
                .get(1)
                .expect("x coord capture group not found")
                .as_str()
                .parse()
                .expect("Unable to parse x coord capture group as int");
            let y: usize = captures
                .get(2)
                .expect("y coord capture group not found")
                .as_str()
                .parse()
                .expect("Unable to parse y coord capture group as int");

            coords.insert((x, y));
        }

        if let Some(captures) = fold_re.captures(&next_line) {
            let direction = captures
                .get(1)
                .expect("Fold direction capture group not found")
                .as_str()
                .chars()
                .nth(0)
                .expect("Error getting first character in fold direction");
            let position = captures
                .get(2)
                .expect("Fold capture group not found")
                .as_str()
                .parse()
                .expect("Unable to parse fold capture group as int");
            folds.push((Direction::from_char(direction), position));
        }
    }

    for next_fold in folds {
        coords = fold(coords, &next_fold);
    }

    let mut max_x = 0;
    let mut max_y = 0;

    for (x, y) in &coords {
        if x > &max_x {
            max_x = *x;
        }

        if y > &max_y {
            max_y = *y;
        }
    }

    let max = cmp::max(max_x, max_y);

    let mut display = vec![vec![' '; max + 1]; max + 1];

    for (x, y) in coords {
        display[x][y] = 'X';
    }

    for a in 0..max_y + 1 {
        for b in 0..max_x {
            print!("{}", display[b][a]);
        }
        println!();
    }
}
