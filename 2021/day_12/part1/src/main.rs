use std::{
    collections::{HashMap, HashSet},
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

use lazy_static::lazy_static;
use regex::Regex;

fn find_paths(
    graph: &HashMap<String, Vec<String>>,
    visited: &mut HashSet<String>,
    path_so_far: &mut Vec<String>,
    next_step: String,
) -> i32 {
    lazy_static! {
        static ref UPPER: Regex = Regex::new(r"[[:upper:]]+").unwrap();
    }
    let mut path_count = 0;

    path_so_far.push(next_step.clone());
    if !UPPER.is_match(&next_step) {
        visited.insert(next_step.clone());
    }

    if !graph.contains_key(&next_step) {
        path_so_far.pop();
        return 0;
    }

    let next_vec = graph.get(&next_step).unwrap();

    for next in next_vec.iter() {
        if next == "end" {
            path_count += 1;
            continue;
        }

        if visited.contains(next) {
            continue;
        }

        path_count += find_paths(&graph, visited, path_so_far, next.clone());
    }

    visited.remove(&next_step);
    path_so_far.pop();

    path_count
}

fn count_paths(graph: HashMap<String, Vec<String>>) -> i32 {
    let start = "start".to_string();
    let mut visited: HashSet<String> = HashSet::new();

    find_paths(&graph, &mut visited, &mut Vec::new(), start)
}

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();

    let mut graph: HashMap<String, Vec<String>> = HashMap::new();

    let re = Regex::new(r"(\w+)-(\w+)").unwrap();

    for line in lines {
        let next_line = line.unwrap();
        let captures = re.captures(&next_line).unwrap();

        let start = captures.get(1).unwrap().as_str().to_string();
        let end = captures.get(2).unwrap().as_str().to_string();

        if !graph.contains_key(&start) {
            graph.insert(start.clone(), Vec::new());
        }

        if !graph.contains_key(&end) {
            graph.insert(end.clone(), Vec::new());
        }

        graph.get_mut(&start).unwrap().push(end.clone());
        graph.get_mut(&end).unwrap().push(start);
    }

    let path_count = count_paths(graph);

    println!("Answer {}", path_count);
}
