use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path, collections::HashSet,
};

fn step(mut map: [[u32; 10]; 10]) -> ([[u32; 10]; 10], u32) {
    let mut flash_count = 0;

    // Step 1 - Increase all elements by 1
    for row in map.iter_mut() {
        for val in row.iter_mut() {
            *val += 1;
        }
    }

    // Step 2 - Propagate flashes

    let mut flashed_points = HashSet::new();
    loop {
        let mut points_to_update: Vec<(usize, usize)> = Vec::new();

        for (idx, row) in map.iter_mut().enumerate() {
            for (idy, val) in row.iter_mut().enumerate() {
                if *val > 9 && !flashed_points.contains(&(idx, idy)){
                    flash_count += 1;

                    // left side
                    if idx > 0 {
                        points_to_update.push((idx - 1, idy));

                        if idy > 0 {
                            points_to_update.push((idx - 1, idy - 1));
                        }

                        if idy < 9 {
                            points_to_update.push((idx - 1, idy + 1));
                        }
                    }

                    // right side
                    if idx < 9 {
                        points_to_update.push((idx + 1, idy));

                        if idy > 0 {
                            points_to_update.push((idx + 1, idy - 1));
                        }

                        if idy < 9 {
                            points_to_update.push((idx + 1, idy + 1));
                        }
                    }

                    // top
                    if idy > 0 {
                        points_to_update.push((idx, idy - 1));
                    }

                    // bottom
                    if idy < 9 {
                        points_to_update.push((idx, idy + 1));
                    }

                    flashed_points.insert((idx, idy));
                }
            }
        }

        if points_to_update.is_empty() {
            break;
        }

        for (x, y) in &points_to_update {
            map[*x][*y] += 1;
        }
    }

    // Step 3 - Ensure all elements that have flashed are reset to 0
    for row in map.iter_mut() {
        for val in row.iter_mut() {
            if *val > 9 {
                *val = 0;
            }
        }
    }

    (map, flash_count)
}

fn print_grid(map: [[u32; 10]; 10]) {
    for idy in 0..10 {
        println!(
            "{} {} {} {} {} {} {} {} {} {}",
            map[0][idy],
            map[1][idy],
            map[2][idy],
            map[3][idy],
            map[4][idy],
            map[5][idy],
            map[6][idy],
            map[7][idy],
            map[8][idy],
            map[9][idy]
        )
    }
}
fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();

    let mut map = [[0; 10]; 10];

    for (idy, line) in lines.enumerate() {
        let next_line = line.unwrap();

        for (idx, c) in next_line.chars().enumerate() {
            let i: u32 = c.to_digit(10).unwrap();
            map[idx][idy] = i;
        }
    }

    let mut count = 0;

    loop {
        let (updated_map, next_flash_count) = step(map);
        map = updated_map;

        count += 1;

        if next_flash_count == 100 {
            break;
        }
    }
    print_grid(map);

    println!("Answer: {}", count);
}
