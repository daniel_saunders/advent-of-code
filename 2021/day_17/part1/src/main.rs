fn step(x_velocity: i32, y_velocity: i32) -> (i32, i32) {
    let new_x_velocity = if x_velocity > 0 {
        x_velocity - 1
    } else if x_velocity < 0 {
        x_velocity + 1
    } else {
        x_velocity
    };

    let new_y_velocity = y_velocity - 1;

    (new_x_velocity, new_y_velocity)
}

fn main() {
    // "target area: x=257..286, y=-101..-57

    let target_x_start = 257;
    let target_x_end = 286;

    let target_y_start = -101;
    let target_y_end = -57;

    let mut best_y = -1000;

    for x_velocity in 0..286 {
        for y_velocity in -101..101 {
            let mut x_position = 0;
            let mut y_position = 0;
            let mut best_y_of_pair = -1000;
            let mut finished = false;
            let mut success = false;
            let mut current_x_velocity = x_velocity;
            let mut current_y_velocity = y_velocity;

            while !finished {
                x_position += current_x_velocity;
                y_position += current_y_velocity;

                if y_position > best_y_of_pair {
                    best_y_of_pair = y_position;
                }

                if x_position >= target_x_start
                    && x_position <= target_x_end
                    && y_position >= target_y_start
                    && y_position <= target_y_end
                {
                    finished = true;
                    success = true
                }

                if x_position > target_x_end || y_position < target_y_end {
                    finished = true;
                }

                let updates = step(current_x_velocity, current_y_velocity);
                current_x_velocity = updates.0;
                current_y_velocity = updates.1;
            }

            if best_y_of_pair > best_y && success == true {
                best_y = best_y_of_pair;
            }
        }
    }

    println!("Answer: {}", best_y);
}
