use std::{fs::File, i32::MAX, io::Read, path::Path};

// 344959 is too low

// 1
// 2
// 3
// 4
// 5

// 1
// 3
// 8
// 13

fn main() {
    let path = Path::new("../input");

    let mut file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let mut input = String::new();

    file.read_to_string(&mut input).unwrap();

    let mut numbers: Vec<i32> = Vec::new();

    let mut max = 0;

    for number in input.split(',') {
        let next_number = number.parse().unwrap();

        if next_number > max {
            max = next_number;
        }

        numbers.push(next_number);
    }

    let mut total_diff = MAX;

    for x in 0..max {
        let mut total = 0;

        for number in &numbers {
            let n = (x - number).abs();
            total += (n * (n + 1)) / 2;
        }

        if total < total_diff {
            total_diff = total;
        }
    }

    println!("Answer: {}", total_diff);
}
