use std::{collections::HashMap, fmt::Display, fs::File, io::Read, path::Path};

struct Generations {
    days: HashMap<i64, i64>,
    current_day: i64,
}

impl Generations {
    fn new(day_counts: [i64; 8]) -> Generations {
        let mut days: HashMap<i64, i64> = HashMap::new();

        for (idx, val) in day_counts.iter().enumerate() {
            days.insert(idx as i64, *val);
        }

        Generations {
            days,
            current_day: 0,
        }
    }

    fn step(&mut self) {
        let next_day = self.days.remove(&self.current_day).unwrap();

        if let None = self.days.get(&(self.current_day + 7)) {
            self.days.insert(self.current_day + 7, 0);
        }

        *self.days.get_mut(&(self.current_day + 7)).unwrap() += next_day;

        self.days.insert(self.current_day + 9, next_day);

        self.current_day += 1;
    }

    fn get_total(&self) -> i64 {
        let mut total = 0;

        for (_, count) in self.days.iter() {
            total += count;
        }

        total
    }
}

impl Display for Generations {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:#?}", self.days)
    }
}

fn main() {
    let path = Path::new("../input");

    let mut file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let mut input: String = String::new();

    file.read_to_string(&mut input)
        .expect("failed to read to string");

    let mut day_counts = [0; 8];

    for next_number in input.split(",") {
        if next_number.len() == 0 {
            continue;
        }

        let next_fish: usize = next_number.parse().unwrap();

        day_counts[next_fish] += 1;
    }

    let mut generations = Generations::new(day_counts);

    for _ in 0..256 {
        generations.step();
    }

    println!("{}", generations.get_total());
}
