use core::fmt;
use std::cmp::{max, min};
use std::convert::TryInto;
use std::io::BufRead;
use std::{fs::File, io::BufReader, path::Path};

use regex::Regex;

struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

fn calculate_overlapping_points(lines: Vec<(Point, Point)>) -> i32 {
    let mut world_space = vec![[0; 1000]; 1000].into_boxed_slice();
    let mut overlapping_points = 0;

    for (first_point, second_point) in lines {
        if first_point.x != second_point.x && first_point.y != second_point.y {
            continue;
        }

        let distance: usize = ((first_point.x - second_point.x).abs()
            + (first_point.y - second_point.y).abs())
        .try_into()
        .unwrap();
        let start_x: usize = min(first_point.x, second_point.x).try_into().unwrap();
        let start_y: usize = min(first_point.y, second_point.y).try_into().unwrap();

        let end_x: usize = max(first_point.x, second_point.x).try_into().unwrap();

        for i in 0..distance + 1 {
            if start_x != end_x {
                world_space[start_x + i][start_y] += 1;
                if world_space[start_x + i][start_y] == 2 {
                    overlapping_points += 1;
                }
            } else {
                world_space[start_x][start_y + i] += 1;
                if world_space[start_x][start_y + i] == 2 {
                    overlapping_points += 1;
                }
            }
        }
    }

    overlapping_points
}

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();

    let re = Regex::new(r"([0-9]+),([0-9]+) -> ([0-9]+),([0-9]+)").unwrap();

    let mut target_lines: Vec<(Point, Point)> = Vec::new();

    for line in lines {
        let next_line = line.expect("Line could not be read, for some crazy reason");
        let captures = re
            .captures(&next_line)
            .expect("Regex does not match all lines");

        let first_point = Point::new(
            captures.get(1).unwrap().as_str().parse().unwrap(),
            captures.get(2).unwrap().as_str().parse().unwrap(),
        );
        let second_point = Point::new(
            captures.get(3).unwrap().as_str().parse().unwrap(),
            captures.get(4).unwrap().as_str().parse().unwrap(),
        );

        target_lines.push((first_point, second_point));
    }

    println!("Answer: {}", calculate_overlapping_points(target_lines));
}
