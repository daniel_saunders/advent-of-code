use core::fmt;
use std::cmp::{max, min};
use std::convert::TryInto;
use std::io::BufRead;
use std::{fs::File, io::BufReader, path::Path};

use regex::Regex;

struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    fn x_u(&self) -> usize {
        self.x.try_into().unwrap()
    }

    fn y_u(&self) -> usize {
        self.y.try_into().unwrap()
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

// (0, 1) (4, 5)
// start x: 0 y: 1

// (4, 1) (0, 5)
// start x: 0 y: 1

// (0, 0) (50, 50)
// (50, 0) (0, 50)

// 23460 too high
fn calculate_overlapping_points(lines: Vec<(Point, Point)>) -> i32 {
    let mut world_space = vec![[0; 1000]; 1000].into_boxed_slice();
    let mut overlapping_points = 0;

    for (first_point, second_point) in lines {
        let mut distance: usize = ((first_point.x - second_point.x).abs()
            + (first_point.y - second_point.y).abs())
        .try_into()
        .unwrap();
        let start_x: usize = min(first_point.x, second_point.x).try_into().unwrap();
        let start_y: usize = min(first_point.y, second_point.y).try_into().unwrap();

        let end_x: usize = max(first_point.x, second_point.x).try_into().unwrap();
        let end_y: usize = max(first_point.y, second_point.y).try_into().unwrap();

        // x - -
        // - x -
        // - - x

        // // Because diagonals are only ever at a 45 degree angle, we can make this assumption
        if start_y != end_y && start_x != end_x {
            distance = end_x - start_x;
        }

        println!("-------------");
        println!("{} -> {}", first_point, second_point);
        for i in 0..distance + 1 {
            if start_y == end_y {
                world_space[start_x + i][start_y] += 1;
                if world_space[start_x + i][start_y] == 2 {
                    overlapping_points += 1;
                }
            } else if start_x == end_x {
                world_space[start_x][start_y + i] += 1;
                if world_space[start_x][start_y + i] == 2 {
                    overlapping_points += 1;
                }
            } else if (start_x == first_point.x_u() && start_y == first_point.y_u()) || (start_x == second_point.x_u() && start_y == second_point.y_u()) {
                world_space[start_x + i][start_y + i] += 1;
                if world_space[start_x + i][start_y + i] == 2 {
                    overlapping_points += 1;
                }
            } else {
                // println!("({}, {}) {}", first_point.x_u() - i, first_point.y_u() + i, distance);
                if (first_point.x > second_point.x) {
                    world_space[first_point.x_u() - i][first_point.y_u() + i] += 1;
                    if world_space[first_point.x_u() - i][first_point.y_u() + i] == 2 {
                        overlapping_points += 1;
                    }
                } else {
                    // println!("wu");
                    world_space[second_point.x_u() - i][second_point.y_u() + i] += 1;
                    if world_space[second_point.x_u() - i][second_point.y_u() + i] == 2 {
                        overlapping_points += 1;
                    }
                }

            }
        }
    }

    overlapping_points
}

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();

    let re = Regex::new(r"([0-9]+),([0-9]+) -> ([0-9]+),([0-9]+)").unwrap();

    let mut target_lines: Vec<(Point, Point)> = Vec::new();

    for line in lines {
        let next_line = line.expect("Line could not be read, for some crazy reason");
        let captures = re
            .captures(&next_line)
            .expect("Regex does not match all lines");

        let first_point = Point::new(
            captures.get(1).unwrap().as_str().parse().unwrap(),
            captures.get(2).unwrap().as_str().parse().unwrap(),
        );
        let second_point = Point::new(
            captures.get(3).unwrap().as_str().parse().unwrap(),
            captures.get(4).unwrap().as_str().parse().unwrap(),
        );

        target_lines.push((first_point, second_point));
    }

    println!("Answer: {}", calculate_overlapping_points(target_lines));
}
