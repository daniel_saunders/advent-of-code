use std::{
    collections::HashSet,
    convert::TryInto,
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

// 988518 too low

fn main() {
    let base: i32 = 10;
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();

    let mut total = 0;

    for line in lines {
        /*
         0000
        1    2
        1    2
         3333
        4    5
        4    5
         6666
         */

        let mut line_posibilities: [HashSet<char>; 7] = [
            vec!['a', 'b', 'c', 'd', 'e', 'f', 'g']
                .into_iter()
                .collect(),
            vec!['a', 'b', 'c', 'd', 'e', 'f', 'g']
                .into_iter()
                .collect(),
            vec!['a', 'b', 'c', 'd', 'e', 'f', 'g']
                .into_iter()
                .collect(),
            vec!['a', 'b', 'c', 'd', 'e', 'f', 'g']
                .into_iter()
                .collect(),
            vec!['a', 'b', 'c', 'd', 'e', 'f', 'g']
                .into_iter()
                .collect(),
            vec!['a', 'b', 'c', 'd', 'e', 'f', 'g']
                .into_iter()
                .collect(),
            vec!['a', 'b', 'c', 'd', 'e', 'f', 'g']
                .into_iter()
                .collect(),
        ];

        let next_line = line.unwrap();

        let (left, right) = next_line.split_once('|').unwrap();

        let left_words = left.split_whitespace();
        let right_words = right.split_whitespace();

        for word in left_words {
            match word.len() {
                2 => {
                    let first_char = word.chars().next().unwrap();
                    let second_char = word.chars().nth(1).unwrap();

                    let mut set = HashSet::new();
                    set.insert(first_char);
                    set.insert(second_char);

                    line_posibilities[2] =
                        line_posibilities[2].intersection(&set).cloned().collect();
                    line_posibilities[5] =
                        line_posibilities[5].intersection(&set).cloned().collect();
                }
                3 => {
                    let first_char = word.chars().next().unwrap();
                    let second_char = word.chars().nth(1).unwrap();
                    let third_char = word.chars().nth(2).unwrap();

                    let mut set = HashSet::new();
                    set.insert(first_char);
                    set.insert(second_char);
                    set.insert(third_char);

                    line_posibilities[0] =
                        line_posibilities[0].intersection(&set).cloned().collect();
                    line_posibilities[2] =
                        line_posibilities[2].intersection(&set).cloned().collect();
                    line_posibilities[5] =
                        line_posibilities[5].intersection(&set).cloned().collect();
                }
                4 => {
                    let first_char = word.chars().next().unwrap();
                    let second_char = word.chars().nth(1).unwrap();
                    let third_char = word.chars().nth(2).unwrap();
                    let fourth_char = word.chars().nth(3).unwrap();

                    let mut set = HashSet::new();
                    set.insert(first_char);
                    set.insert(second_char);
                    set.insert(third_char);
                    set.insert(fourth_char);

                    line_posibilities[1] =
                        line_posibilities[1].intersection(&set).cloned().collect();
                    line_posibilities[2] =
                        line_posibilities[2].intersection(&set).cloned().collect();
                    line_posibilities[3] =
                        line_posibilities[3].intersection(&set).cloned().collect();
                    line_posibilities[5] =
                        line_posibilities[5].intersection(&set).cloned().collect();
                }
                5 => {
                    let first_char = word.chars().next().unwrap();
                    let second_char = word.chars().nth(1).unwrap();
                    let third_char = word.chars().nth(2).unwrap();
                    let fourth_char = word.chars().nth(3).unwrap();
                    let fifth_char = word.chars().nth(4).unwrap();

                    let mut set = HashSet::new();
                    set.insert(first_char);
                    set.insert(second_char);
                    set.insert(third_char);
                    set.insert(fourth_char);
                    set.insert(fifth_char);

                    line_posibilities[0] =
                        line_posibilities[0].intersection(&set).cloned().collect();
                    line_posibilities[3] =
                        line_posibilities[3].intersection(&set).cloned().collect();
                    line_posibilities[6] =
                        line_posibilities[6].intersection(&set).cloned().collect();
                }
                6 => {
                    let first_char = word.chars().next().unwrap();
                    let second_char = word.chars().nth(1).unwrap();
                    let third_char = word.chars().nth(2).unwrap();
                    let fourth_char = word.chars().nth(3).unwrap();
                    let fifth_char = word.chars().nth(4).unwrap();
                    let sixth_char = word.chars().nth(5).unwrap();

                    let mut set = HashSet::new();
                    set.insert(first_char);
                    set.insert(second_char);
                    set.insert(third_char);
                    set.insert(fourth_char);
                    set.insert(fifth_char);
                    set.insert(sixth_char);

                    line_posibilities[0] =
                        line_posibilities[0].intersection(&set).cloned().collect();
                    line_posibilities[1] =
                        line_posibilities[1].intersection(&set).cloned().collect();
                    line_posibilities[5] =
                        line_posibilities[5].intersection(&set).cloned().collect();
                    line_posibilities[6] =
                        line_posibilities[6].intersection(&set).cloned().collect();
                }
                _ => (),
            }
        }

        let mut answers: HashSet<char> = HashSet::new();

        while line_posibilities.iter().any(|set| set.len() != 1) {
            let mut new_answer = 'x';
            let mut new_answer_index = 0;

            for (idx, line) in line_posibilities.iter().enumerate() {
                if line.len() == 1 {
                    let possible_answer = *line.iter().last().unwrap();
                    if !answers.contains(&possible_answer) {
                        new_answer = *line.iter().last().unwrap();
                        new_answer_index = idx;
                        answers.insert(new_answer);
                        break;
                    }
                }
            }

            for x in 0..line_posibilities.len() {
                if x == new_answer_index {
                    continue;
                }

                line_posibilities[x].remove(&new_answer);
            }
        }

        let digits: Vec<char> = line_posibilities
            .iter()
            .map(|set| *set.iter().last().unwrap())
            .collect();

        let mut new_number = 0;

        for (idx, word) in right_words.enumerate() {
            let coefficient = match word.len() {
                2 => 1,
                3 => 7,
                4 => 4,
                5 => {
                    if word.contains(digits[1]) {
                        5
                    } else if word.contains(digits[4]) {
                        2
                    } else {
                        3
                    }
                }
                6 => {
                    if !word.contains(digits[3]) {
                        0
                    } else if !word.contains(digits[2]) {
                        6
                    } else {
                        9
                    }
                }
                7 => 8,
                _ => panic!("not possible!"),
            };

            let exp: u32 = (3 - idx).try_into().unwrap();
            new_number += coefficient * base.pow(exp);
        }

        total += new_number;
    }

    println!("Answer: {}", total);
}
