use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();

    let mut digit_counts = [0; 10];

    for line in lines {
        let next_line = line.unwrap();

        let (left, right) = next_line.split_once('|').unwrap();

        let left_words = left.split(' ');
        let right_words = right.split(' ');

        for word in right_words {
            match word.len() {
                2 => digit_counts[1] += 1,
                3 => digit_counts[7] += 1,
                4 => digit_counts[4] += 1,
                7 => digit_counts[8] += 1,
                _ => (),
            }
        }
    }

    let answer: i32 = digit_counts.iter().sum();

    println!("Answer: {}", answer);
}
