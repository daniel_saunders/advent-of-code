use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

use regex::Regex;

type Rules = HashMap<char, HashMap<char, char>>;

fn update_string(rules: &Rules, original_string: String) -> String {
    let mut new_string: String = "".to_string();

    let mut previous_char = '_';

    for c in original_string.chars() {
        if let Some(map) = rules.get(&previous_char) {
            if let Some(new_char) = map.get(&c) {
                new_string.push(new_char.clone());
            }
        }

        new_string.push(c);
        previous_char = c;
    }

    new_string
}

fn count_chars(string: String) -> HashMap<char, i32> {
    let mut counts = HashMap::new();

    for c in string.chars() {
        if !counts.contains_key(&c) {
            counts.insert(c, 0);
        }

        *counts.get_mut(&c).unwrap() += 1;
    }

    counts
}

fn get_answer(counts: HashMap<char, i32>) -> i32 {
    let mut mce_count = 0;
    let mut lce_count = 1000000;

    for (_, count) in counts {
        if count > mce_count {
            mce_count = count;
        }

        if count < lce_count {
            lce_count = count;
        }
    }

    mce_count - lce_count
}

fn main() {
    let path = Path::new("../input");

    let file = File::open(path).expect("Could not open file");

    let lines = BufReader::new(file).lines();

    let mut rules: Rules = HashMap::new();
    let mut original_string: String = "".to_string();

    let mut is_first_line = true;

    let rule_re = Regex::new(r"([A-Z])([A-Z]) -> ([A-Z])").expect("Unable to build rule regex");

    for line in lines {
        let next_line = line.expect("Unable to read line from input file");

        if is_first_line {
            original_string = next_line;

            is_first_line = false;
            continue;
        }

        if next_line.len() == 0 {
            continue;
        }

        let captures = rule_re
            .captures(&next_line)
            .expect("Unable to read regex captures for rule");

        let first_letter = captures
            .get(1)
            .expect("Unable to get first capture group for rule")
            .as_str()
            .chars()
            .nth(0)
            .expect("Unable to get first character");

        let second_letter = captures
            .get(2)
            .expect("Unable to get first capture group for rule")
            .as_str()
            .chars()
            .nth(0)
            .expect("Unable to get first character");

        let insert_letter = captures
            .get(3)
            .expect("Unable to get first capture group for rule")
            .as_str()
            .chars()
            .nth(0)
            .expect("Unable to get first character");

        if !rules.contains_key(&first_letter) {
            rules.insert(first_letter, HashMap::new());
        }

        rules
            .get_mut(&first_letter)
            .unwrap()
            .insert(second_letter, insert_letter);
    }

    for _ in 0..10 {
        original_string = update_string(&rules, original_string)
    }

    let counts = count_chars(original_string);

    let answer = get_answer(counts);

    println!("{}", answer);
}
