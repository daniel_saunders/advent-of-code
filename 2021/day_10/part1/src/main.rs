use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

fn get_invalid_char_cost(c: char) -> i32 {
    match c {
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
        _ => panic!("Unexpected invalid character {}", c),
    }
}

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();

    let mut total_points = 0;

    for line in lines {
        let next_line = line.unwrap();

        let mut stack: Vec<char> = Vec::new();

        for c in next_line.chars() {
            match c {
                '(' | '[' | '{' | '<' => {
                    stack.push(c);
                    continue;
                }
                _ => (),
            }

            if let Some(top) = stack.pop() {
                match top {
                    '(' => {
                        if c != ')' {
                            total_points += get_invalid_char_cost(c);
                            break;
                        }
                    }
                    '[' => {
                        if c != ']' {
                            total_points += get_invalid_char_cost(c);
                            break;
                        }
                    }
                    '{' => {
                        if c != '}' {
                            total_points += get_invalid_char_cost(c);
                            break;
                        }
                    }
                    '<' => {
                        if c != '>' {
                            total_points += get_invalid_char_cost(c);
                            break;
                        }
                    }
                    _ => stack.push(c),
                }
            } else {
                total_points += get_invalid_char_cost(c);
            }
        }
    }

    println!("Answer: {}", total_points);
}
