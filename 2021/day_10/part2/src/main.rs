use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();
    let mut scores = Vec::new();

    for line in lines {
        let next_line = line.unwrap();

        let mut stack: Vec<char> = Vec::new();
        let mut valid = true;

        for c in next_line.chars() {
            match c {
                '(' | '[' | '{' | '<' => {
                    stack.push(c);
                    continue;
                }
                _ => (),
            }

            if let Some(top) = stack.pop() {
                match top {
                    '(' => {
                        if c != ')' {
                            valid = false;
                            break;
                        }
                    }
                    '[' => {
                        if c != ']' {
                            valid = false;
                            break;
                        }
                    }
                    '{' => {
                        if c != '}' {
                            valid = false;
                            break;
                        }
                    }
                    '<' => {
                        if c != '>' {
                            valid = false;
                            break;
                        }
                    }
                    _ => stack.push(c),
                }
            } else {
                valid = false;
                break;
            }
        }

        if stack.len() > 0 && valid {
            let mut next_score: i64 = 0;

            for c in stack.iter().rev() {
                print!("{}", c);
                match c {
                    '(' => next_score = next_score * 5 + 1,
                    '[' => next_score = next_score * 5 + 2,
                    '{' => next_score = next_score * 5 + 3,
                    '<' => next_score = next_score * 5 + 4,
                    _ => panic!("Unrecognized character on stack {}", c),
                }
            }

            println!(" {}", next_score);
            scores.push(next_score);
        }
    }

    scores.sort();
    println!("{:?}", scores);

    println!("Answer: {}", scores[scores.len() / 2]);
}
