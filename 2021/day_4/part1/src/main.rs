use std::{
    fs::File,
    io::{BufRead, BufReader, Error},
    path::Path,
};

fn parse_numbers(line: Result<String, Error>, mut numbers: Vec<i32>) -> Vec<i32> {
    for number in line.unwrap().split(",") {
        let number_i32: i32 = number.parse().unwrap();

        numbers.push(number_i32);
    }

    numbers
}

fn is_a_win(board: [[i32; 5]; 5], newest_x: usize, newest_y: usize) -> bool {
    let mut all_true = true;
    for x in 0..5 {
        if board[x][newest_y] != -1 {
            all_true = false;
            break;
        }
    }

    if all_true {
        return all_true;
    }

    let mut all_true = true;
    for y in 0..5 {
        if board[newest_x][y] != -1 {
            all_true = false;
            break;
        }
    }

    if all_true {
        return all_true;
    }

    false
}

fn find_winning_board(
    numbers: Vec<i32>,
    mut boards: Vec<[[i32; 5]; 5]>,
) -> Option<([[i32; 5]; 5], i32)> {
    for number in numbers {
        for i in 0..5 {
            for j in 0..5 {
                for board_idx in 0..boards.len() {
                    if boards[board_idx][i][j] == number {
                        boards[board_idx][i][j] = -1;

                        if is_a_win(boards[board_idx], i, j) {
                            return Some((boards[board_idx].clone(), number));
                        }
                    }
                }
            }
        }
    }

    None
}

fn calculate_final_number(board: [[i32; 5]; 5], number: i32) -> i32 {
    let mut total = 0;

    for i in 0..5 {
        for j in 0..5 {
            if board[i][j] != -1 {
                total += board[i][j];
            }
        }
    }

    return total * number;
}

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();

    let mut numbers: Vec<i32> = Vec::new();

    let mut boards: Vec<[[i32; 5]; 5]> = Vec::new();

    let mut next_board = [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
    ];

    let mut board_idx = 0;
    for (idx, line) in lines.enumerate() {
        if idx == 0 {
            numbers = parse_numbers(line, numbers);
            continue;
        }

        let next_line = line.unwrap();
        if next_line.len() == 0 {
            board_idx = 0;
            continue;
        }

        let mut line_idx = 0;
        for number in next_line.split(" ") {
            if number.len() == 0 {
                continue;
            }

            next_board[board_idx][line_idx] = number.parse().unwrap();
            line_idx += 1;
        }

        if board_idx == 4 {
            boards.push(next_board.clone())
        }

        board_idx += 1;
    }

    let final_number = match find_winning_board(numbers, boards) {
        Some((board, number)) => calculate_final_number(board, number),
        None => panic!("missing"),
    };

    println!("{}", final_number);
}
