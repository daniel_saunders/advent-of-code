use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let mut map: Vec<Vec<u32>> = Vec::new();

    let lines = BufReader::new(file).lines();

    let mut total_risk = 0;

    for (idx, line) in lines.enumerate() {
        let next_line = match line {
            Ok(next_line) => next_line,
            Err(why) => panic!("{}", why),
        };

        map.push(Vec::new());

        for c in next_line.chars() {
            let next_position = c.to_digit(10).unwrap();

            map[idx].push(next_position);
        }
    }

    for (idx, row) in map.iter().enumerate() {
        for (idy, pos) in row.iter().enumerate() {
            let mut is_low_point = true;
            if idx != 0 && map[idx - 1][idy] <= *pos {
                is_low_point = false;
            }

            if idy != 0 && map[idx][idy - 1] <= *pos {
                is_low_point = false;
            }

            if idx != map.len() - 1 && map[idx + 1][idy] <= *pos {
                is_low_point = false;
            }

            if idy != row.len() - 1 && map[idx][idy + 1] <= *pos {
                is_low_point = false;
            }

            if is_low_point {
                total_risk += *pos + 1;
            }
        }
    }

    println!("Answer: {}", total_risk);
}
