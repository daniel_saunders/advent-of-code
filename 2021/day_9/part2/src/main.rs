use std::{
    collections::{HashSet, VecDeque},
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

fn determine_low_points(map: &Vec<Vec<u32>>) -> Vec<(usize, usize)> {
    let mut low_points = Vec::new();

    for (idx, row) in map.iter().enumerate() {
        for (idy, pos) in row.iter().enumerate() {
            let mut is_low_point = true;
            if idx != 0 && map[idx - 1][idy] <= *pos {
                is_low_point = false;
            }

            if idy != 0 && map[idx][idy - 1] <= *pos {
                is_low_point = false;
            }

            if idx != map.len() - 1 && map[idx + 1][idy] <= *pos {
                is_low_point = false;
            }

            if idy != row.len() - 1 && map[idx][idy + 1] <= *pos {
                is_low_point = false;
            }

            if is_low_point {
                low_points.push((idx, idy));
            }
        }
    }

    return low_points;
}

fn calculate_basin_size(map: &Vec<Vec<u32>>, coord: (usize, usize)) -> u32 {
    let row_length = map[0].len();
    let mut search_queue: VecDeque<(usize, usize)> = VecDeque::new();
    let mut seen_set: HashSet<(usize, usize)> = HashSet::new();

    search_queue.push_back(coord);
    seen_set.insert(coord);

    let mut basin_size = 1;

    while !search_queue.is_empty() {
        let next_point = search_queue.pop_front().unwrap();
        let x = next_point.0;
        let y = next_point.1;

        if x != 0
            && map[x - 1][y] > map[x][y]
            && map[x - 1][y] != 9
            && !seen_set.contains(&(x - 1, y))
        {
            seen_set.insert((x - 1, y));
            search_queue.push_back((x - 1, y));
            basin_size += 1;
        }

        if y != 0
            && map[x][y - 1] > map[x][y]
            && map[x][y - 1] != 9
            && !seen_set.contains(&(x, y - 1))
        {
            seen_set.insert((x, y - 1));
            search_queue.push_back((x, y - 1));
            basin_size += 1;
        }

        if x != map.len() - 1
            && map[x + 1][y] > map[x][y]
            && map[x + 1][y] != 9
            && !seen_set.contains(&(x + 1, y))
        {
            seen_set.insert((x + 1, y));
            search_queue.push_back((x + 1, y));
            basin_size += 1;
        }

        if y != row_length - 1
            && map[x][y + 1] > map[x][y]
            && map[x][y + 1] != 9
            && !seen_set.contains(&(x, y + 1))
        {
            seen_set.insert((x, y + 1));
            search_queue.push_back((x, y + 1));
            basin_size += 1;
        }
    }

    basin_size
}

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let mut map: Vec<Vec<u32>> = Vec::new();

    let lines = BufReader::new(file).lines();

    for (idx, line) in lines.enumerate() {
        let next_line = match line {
            Ok(next_line) => next_line,
            Err(why) => panic!("{}", why),
        };

        map.push(Vec::new());

        for c in next_line.chars() {
            let next_position = c.to_digit(10).unwrap();

            map[idx].push(next_position);
        }
    }

    let low_points = determine_low_points(&map);

    let mut basin_sizes: Vec<u32> = low_points
        .iter()
        .map(|point| calculate_basin_size(&map, point.clone()))
        .collect();
    basin_sizes.sort();

    let answer = basin_sizes[basin_sizes.len() - 1]
        * basin_sizes[basin_sizes.len() - 2]
        * basin_sizes[basin_sizes.len() - 3];

    println!("Answer: {}", answer);
}
