use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::path::Path;
use regex::Regex;

struct Position {
    x: i32,
    y: i32,
    aim: i32
}

impl Position {
    pub fn new() -> Position {
        Position{ x: 0, y: 0, aim: 0 }
    }

    pub fn forward(&mut self, distance: i32) {
        self.x += distance;
        self.y += self.aim * distance;
    }

    pub fn backward(&mut self, distance: i32) {
        self.x -= distance;
    }

    pub fn up(&mut self, distance: i32) {
        self.aim -= distance;
    }

    pub fn down(&mut self, distance: i32) {
        self.aim += distance;
    }

    pub fn multiply(&self) -> i32 {
        self.x * self.y
    }
}

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why)
    };

    let lines = BufReader::new(file).lines();
    let re = Regex::new(r"^([a-zA-Z]+) ([0-9]+)$").unwrap();

    let mut position = Position::new();

    for line in lines {
        let next_line = line.unwrap();
        let captures = re.captures(&next_line).unwrap();
        let command = captures.get(1).unwrap().as_str();
        let distance: i32 = captures.get(2).unwrap().as_str().parse().unwrap();


        match command {
            "forward" => position.forward(distance),
            "backward" => position.backward(distance),
            "up" => position.up(distance),
            "down" => position.down(distance),
            _ => panic!("Unrecognized command!")
        };
    }

    println!("{}", position.multiply());
}
