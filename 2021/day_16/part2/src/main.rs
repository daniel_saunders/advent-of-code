use std::{
    cmp,
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

struct Bits {
    bytes: Vec<u8>,
    position: (usize, usize),
}

impl Bits {
    fn new(bytes: Vec<u8>) -> Bits {
        Bits {
            bytes,
            position: (0, 0),
        }
    }
    fn next_bits(&mut self, count: usize) -> u128 {
        let mut internal_count = count.clone();
        let mut return_value: u128 = 0;

        while internal_count > 0 {
            let next_step = cmp::min(internal_count, 8 - self.position.1);
            return_value = return_value << next_step;
            let change = self.bytes[self.position.0] << self.position.1
                >> self.position.1
                >> 8 - next_step - self.position.1;
            return_value += change as u128;

            self.position.1 += next_step;
            if self.position.1 == 8 {
                self.position.0 += 1;
                self.position.1 = 0;
            }

            internal_count -= next_step;
        }

        return_value
    }
}

fn type_0_read(bits: &mut Bits, values: &mut Vec<u128>) -> usize {
    let mut consumed_bits = 4;
    let length = bits.next_bits(15);
    consumed_bits += 15;

    let mut sub_consumed_bits = 0;
    while sub_consumed_bits < length as usize {
        let response = read_packet(bits);
        consumed_bits += response.0;
        sub_consumed_bits += response.0;
        values.push(response.1);
    }

    consumed_bits
}

fn type_1_read(bits: &mut Bits, values: &mut Vec<u128>) -> usize {
    let mut consumed_bits = 4;
    let sub_packet_count = bits.next_bits(11);
    consumed_bits += 11;

    let mut steps = 0;
    while steps < sub_packet_count {
        let response = read_packet(bits);
        consumed_bits += response.0;
        steps += 1;
        values.push(response.1);
    }

    consumed_bits
}

fn read_sub_packets(bits: &mut Bits, values: &mut Vec<u128>) -> usize {
    match bits.next_bits(1) {
        0 => type_0_read(bits, values),
        1 => type_1_read(bits, values),
        _ => panic!("-- Impossible value for length type ID"),
    }
}

fn read_packet(bits: &mut Bits) -> (usize, u128) {
    let mut consumed_bits = 0;
    // version
    let value;
    bits.next_bits(3);
    consumed_bits += 3;

    let mut values: Vec<u128> = Vec::new();
    match bits.next_bits(3) {
        4 => {
            consumed_bits += 3;

            let mut number = 0;
            let mut finished = false;
            while !finished {
                let next_block = bits.next_bits(5);
                consumed_bits += 5;
                finished = next_block & 0b10000 == 0;

                number <<= 4;
                number += next_block & 0b01111;
            }

            value = number;
        }
        0 => {
            consumed_bits += read_sub_packets(bits, &mut values);

            value = values.iter().sum();
        }
        1 => {
            consumed_bits += read_sub_packets(bits, &mut values);

            value = values.iter().product();
        }
        2 => {
            consumed_bits += read_sub_packets(bits, &mut values);

            value = *values.iter().min().unwrap();
        }
        3 => {
            consumed_bits += read_sub_packets(bits, &mut values);

            value = *values.iter().max().unwrap();
        }
        5 => {
            consumed_bits += read_sub_packets(bits, &mut values);

            value = if values.get(0) > values.get(1) { 1 } else { 0 }
        }
        6 => {
            consumed_bits += read_sub_packets(bits, &mut values);

            value = if values.get(0) < values.get(1) { 1 } else { 0 }
        }
        7 => {
            consumed_bits += read_sub_packets(bits, &mut values);

            value = if values.get(0) == values.get(1) { 1 } else { 0 }
        }
        _ => panic!("Type not yet recognized"),
    }

    (consumed_bits, value)
}

fn main() {
    let path = Path::new("../input");

    let file = File::open(path).expect("Unable to open input file");

    let mut line: String = "".to_string();

    BufReader::new(file)
        .read_line(&mut line)
        .expect("Unable to read line from input file");

    let bytes = hex::decode(line).expect("Unable to decode hexadecimal characters");
    // let bytes = hex::decode("9C0141080250320F1802104A08").expect("Unable to decode hexadecimal characters");
    let mut bits = Bits::new(bytes);

    let response = read_packet(&mut bits);
    println!("Result: {}", response.1);
}
