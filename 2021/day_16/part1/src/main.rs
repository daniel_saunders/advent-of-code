use std::{
    cmp,
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

struct Bits {
    bytes: Vec<u8>,
    position: (usize, usize),
}

impl Bits {
    fn new(bytes: Vec<u8>) -> Bits {
        Bits {
            bytes,
            position: (0, 0),
        }
    }
    fn next_bits(&mut self, count: usize) -> u128 {
        let mut internal_count = count.clone();
        let mut return_value: u128 = 0;

        println!("----");
        while internal_count > 0 {
            let next_step = cmp::min(internal_count, 8 - self.position.1);
            return_value = return_value << next_step;
            println!(
                "Requested: {}; current position is: {}",
                count, self.position.1
            );
            println!("We determined that we need {} blocks from this", next_step);
            println!(
                "{:#b} {:#b} {:#b} {:#b} {:#b}",
                return_value,
                self.bytes[self.position.0],
                self.bytes[self.position.0] << self.position.1,
                self.bytes[self.position.0] << self.position.1 >> self.position.1,
                self.bytes[self.position.0] << self.position.1
                    >> self.position.1
                    >> 8 - next_step - self.position.1,
            );
            let change = self.bytes[self.position.0] << self.position.1
                >> self.position.1
                >> 8 - next_step - self.position.1;
            return_value += change as u128;

            self.position.1 += next_step;
            if self.position.1 == 8 {
                self.position.0 += 1;
                self.position.1 = 0;
            }

            internal_count -= next_step;
        }

        println!("We are returning {} ({:#b})", return_value, return_value);
        return_value
    }
}

fn read_packet(bits: &mut Bits) -> (usize, u128) {
    let mut consumed_bits = 0;
    let mut version_sum = bits.next_bits(3);
    println!("  >> Next version {}", version_sum);
    consumed_bits += 3;

    match bits.next_bits(3) {
        4 => {
            consumed_bits += 3;
            println!("-- Found literal packet");

            let mut number = 0;
            let mut finished = false;
            while !finished {
                let next_block = bits.next_bits(5);
                consumed_bits += 5;
                println!("Comparing {:#b} {:#b}", next_block, next_block & 0b10000);
                finished = next_block & 0b10000 == 0;

                number <<= 4;
                number += next_block & 0b01111;
            }

            println!("-- number {}", number);
        }
        _ => match bits.next_bits(1) {
            0 => {
                consumed_bits += 4;
                let length = bits.next_bits(15);
                println!(
                    "-- Found packet with type ID 0; sub packets are length {} in bits",
                    length
                );
                consumed_bits += 15;

                let mut sub_consumed_bits = 0;
                while sub_consumed_bits < length as usize {
                    let response = read_packet(bits);
                    version_sum += response.1;
                    consumed_bits += response.0;
                    sub_consumed_bits += response.0;
                    println!("** Now at {} bits", sub_consumed_bits);
                }
            }
            1 => {
                consumed_bits += 4;
                let sub_packet_count = bits.next_bits(11);
                println!(
                    "-- Found packet with type ID 1; sub packet count is {}",
                    sub_packet_count
                );
                consumed_bits += 11;

                let mut steps = 0;
                while steps < sub_packet_count {
                    let response = read_packet(bits);
                    version_sum += response.1;
                    consumed_bits += response.0;
                    steps += 1;
                    println!("** Now at {} packets", steps);
                }
            }
            _ => panic!("-- Impossible value for length type ID"),
        },
    }

    (consumed_bits, version_sum)
}

fn main() {
    let path = Path::new("../input");

    let file = File::open(path).expect("Unable to open input file");

    let mut line: String = "".to_string();

    BufReader::new(file)
        .read_line(&mut line)
        .expect("Unable to read line from input file");

    let bytes = hex::decode(line).expect("Unable to decode hexadecimal characters");
    let mut bits = Bits::new(bytes);

    let response = read_packet(&mut bits);
    println!("Sum: {}", response.1);
}
