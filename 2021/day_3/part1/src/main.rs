use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();

    let mut column_counts = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];

    for line in lines {
        let next_line = match line {
            Ok(next_line) => next_line,
            Err(why) => panic!("{}", why),
        };

        for (ix, ch) in next_line.chars().enumerate() {
            if ch == '0' {
                column_counts[0][ix] += 1;
            } else {
                column_counts[1][ix] += 1;
            }
        }
    }

    let mut most_common: u32 = 0;
    let mut least_common: u32 = 0;

    for idx in 0..12 {
        if column_counts[0][idx] > column_counts[1][idx] {
            most_common *= 2;
            least_common = least_common * 2 + 1;
        } else {
            least_common *= 2;
            most_common = most_common * 2 + 1;
        }
    }

    println!("{}", most_common * least_common);
}
