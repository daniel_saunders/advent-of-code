use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Ok(file) => file,
        Err(why) => panic!("{}", why),
    };

    let lines = BufReader::new(file).lines();

    let mut least_common_words: Vec<String> = Vec::new();
    let mut most_common_words: Vec<String> = Vec::new();

    for line in lines {
        let next_line = match line {
            Ok(next_line) => next_line,
            Err(why) => panic!("{}", why),
        };

        most_common_words.push(next_line.clone());
        least_common_words.push(next_line);
    }

    let mut current_index = 0;
    while least_common_words.len() > 1 {
        let mut column_counts = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ];

        for next_line in &least_common_words {
            for (ix, ch) in next_line.chars().enumerate() {
                if ch == '0' {
                    column_counts[0][ix] += 1;
                } else {
                    column_counts[1][ix] += 1;
                }
            }
        }

        let next_value = if column_counts[0][current_index] > column_counts[1][current_index] {
            '1'
        } else {
            '0'
        };

        least_common_words.retain(|word| word.chars().nth(current_index).unwrap() == next_value);
        current_index += 1;
    }

    let mut current_index = 0;
    while most_common_words.len() > 1 {
        let mut column_counts = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ];

        for next_line in &most_common_words {
            for (ix, ch) in next_line.chars().enumerate() {
                if ch == '0' {
                    column_counts[0][ix] += 1;
                } else {
                    column_counts[1][ix] += 1;
                }
            }
        }

        let next_value = if column_counts[0][current_index] > column_counts[1][current_index] {
            '0'
        } else {
            '1'
        };

        most_common_words.retain(|word| word.chars().nth(current_index).unwrap() == next_value);
        current_index += 1;
    }

    assert!(least_common_words.len() == 1);
    assert!(most_common_words.len() == 1);

    let final_value = u32::from_str_radix(least_common_words.get(0).unwrap(), 2).unwrap()
        * u32::from_str_radix(most_common_words.get(0).unwrap(), 2).unwrap();

    println!("{}", final_value);
}
