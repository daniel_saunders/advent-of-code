use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::path::Path;

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Err(why) => panic!("{}", why),
        Ok(file) => file
    };

    let lines = BufReader::new(file).lines();

    let mut count = 0;
    let mut last_value = 100000;

    for line in lines {
        if let Ok(next_line) = line {
            let current_value: i32 = next_line.parse().unwrap();

            if current_value > last_value {
                count += 1;
            }

            last_value = current_value;
        }
    }

    println!("count: {}", count);
}
