use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::path::Path;

struct SlidingWindow {
    current_index: usize,
    window: [i32; 3]
}

impl SlidingWindow {
    pub fn new() -> SlidingWindow {
        SlidingWindow { current_index: 0, window: [10000, 10000, 10000] }
    }

    pub fn add(&mut self, val: i32) {
        println!("{}", self.current_index);
        self.window[self.current_index] = val;

        self.current_index = (self.current_index + 1) % 3;
    }

    pub fn total(&self) -> i32 {
        return self.window[0] + self.window[1] + self.window[2];
    }
}

fn main() {
    let path = Path::new("../input");

    let file = match File::open(path) {
        Err(why) => panic!("{}", why),
        Ok(file) => file
    };

    let lines = BufReader::new(file).lines();

    let mut count = 0;
    let mut last_value = 2147483647;

    let mut sliding_window = SlidingWindow::new();

    for line in lines {
        if let Ok(next_line) = line {
            let current_value: i32 = next_line.parse().unwrap();

            sliding_window.add(current_value);

            if sliding_window.total() > last_value {
                count += 1;
            }

            last_value = sliding_window.total();
        }
    }

    println!("count: {}", count);
}
